export default ({ env }) => ({
  connection: {
    client: "postgres",
    connection: {
      host: env("DATABASE_HOST", "containers-us-west-119.railway.app"),
      port: env.int("DATABASE_PORT", 7623),
      database: env("DATABASE_NAME", "railway"),
      user: env("DATABASE_USERNAME", "postgres"),
      password: env("DATABASE_PASSWORD", "Q77OsBHdLUN0YqgYNv8E"),
      ssl: env.bool("DATABASE_SSL", false),
    },
  },
});
